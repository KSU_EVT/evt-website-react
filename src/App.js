import React from 'react';
import { useRoutes } from 'hookrouter';
import routes from './routes/routes';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

const App = () => <div className="cotainer-fluid App">{useRoutes(routes)}</div>;

export default App;
