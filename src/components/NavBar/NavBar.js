import React from 'react';
import NavBarView from './NavBarView';
import * as Images from '../../constants/images';

const NavBar = () => <NavBarView logo={Images.TeamLogo} />;

export default NavBar;
