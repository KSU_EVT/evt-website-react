import React from 'react';
import PropTypes from 'prop-types';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faSearch, faTags } from '@fortawesome/free-solid-svg-icons';
import './NavBar.css';

const NavbarView = props => (
  <div className="navbar">
    <a href="home">
      <img src={props.logo} alt="EVT Logo" className="logo" />
    </a>

    <ul className="nav">
      <li>
        <a className="nav-link" href="about">
          About
        </a>
      </li>
      <li>
        <a className="nav-link" href="blog">
          Blog
        </a>
      </li>
      <li>
        <a className="nav-link" href="calendar">
          Calendar
        </a>
      </li>
      <li>
        <a className="nav-link" href="contact us">
          Contact Us
        </a>
      </li>
      <li>
        <a className="nav-link" href="support">
          Support
        </a>
      </li>
      {/* <li>
        <a className="nav-link" href="#"><FontAwesomeIcon icon={faSearch}/></a>
      </li>
      <li>
        <a className="nav-link" href="#"><FontAwesomeIcon icon={faTags}/></a>
      </li> */}
    </ul>
  </div>
);

NavbarView.propTypes = {
  logo: PropTypes.string,
};

export default NavbarView;
