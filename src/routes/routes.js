import React from 'react';
import LandingPage from './LandingPage';

const routes = {
  '/': () => <LandingPage />,
  '/*': () => <LandingPage />,
};

export default routes;
