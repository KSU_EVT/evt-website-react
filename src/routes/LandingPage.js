import React from 'react';
import { Navbar } from '../components/NavBar';

const LandingPage = () => (
  <div className="container-fluid">
    <Navbar />
  </div>
);

export default LandingPage;
